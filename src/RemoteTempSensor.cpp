#include <Arduino.h>
#include "WiFiManager.h"
#include "webServer.h"
#include <DallasTemperature.h>
#include "configManager.h"
#include <OneWire.h>
#include <Ticker.h>
#include <WebSocketsClient.h>
#include <AsyncMqtt_Generic.h>
#include "statusStruct.h"
#include "ArduinoJson.h"

// CAUTION: - OLD AFTERBURNERS DON'T RESPOND TO PINGS (prior V3.5.1.40 at least) :( 
// this causes unwanted reconnects, which refreshes the JSON to all clients.
// THAT then makes the BT App / web page difficult to work with as user entered settings will get clobbered.
//#define ENABLE_WEBSOCKET_PING  

// These defines must be put before #include <ESP_DoubleResetDetector.h>
// to select where to store DoubleResetDetector's variable.
// For ESP8266, You must select one to be true (RTC, EEPROM, LITTLEFS or SPIFFS)
// Otherwise, library will use default EEPROM storage
#ifdef ESP8266
  #define ESP8266_DRD_USE_RTC     true
#endif
#define ESP_DRD_USE_LITTLEFS    false
#define ESP_DRD_USE_SPIFFS      false
#define ESP_DRD_USE_EEPROM      false
// Number of seconds after reset during which a 
// subsequent reset will be considered a double reset.
#define DRD_TIMEOUT 5
// RTC Memory Address for the DoubleResetDetector to use
#define DRD_ADDRESS 0
// Uncomment to have debug
//#define DOUBLERESETDETECTOR_DEBUG       true
#include <ESP_DoubleResetDetector.h>      //https://github.com/khoih-prog/ESP_DoubleResetDetector

#define LED_ON      LOW
#define LED_OFF     HIGH
#define USE_WEBSOCKET

// PINS
const int pinLED = 2;            // D4 on NodeMCU - FFS!
const int pinDS18B20sensor = 0;  // D3 on NodeMCU - FFS!

DoubleResetDetector* pDRD;
bool DRD_Detected = false;
bool ledActive = false;
int ledCount = -1;
unsigned long specialFlash = 0;
bool useWebSocket = true;

//for LED status
Ticker ticker;
Ticker mqttReconnectTimer;

// Temperature Setup
OneWire oneWire(pinDS18B20sensor);
DallasTemperature sensors(&oneWire);

WebSocketsClient webSocket;
AsyncMqttClient mqttClient;

sStatusStruct statusInfo;
bool reqReading = false;

unsigned long previousSample = 0;
unsigned long ledMillis = 0;
const int sampleInterval = 4000;

// operational parameters
bool websocketUse = true;
char websocketIP[32];
char mqttBroker[32];
uint16_t mqttPort;
char mqttUser[32];
char mqttPass[40];
char mqttTopic[40];
String otaTopic = "TempRemote/ota";
String statusTopic = "TempRemote/Status";
String cmdTopic = "/cmd";
String onlineTopic = "/status";

void checkLED(bool force=false);
float getReading();
void checkReading();
bool hasSSID();
void setupWebSocket();
void forgetConfigCallback();
void configChangeCallback();
void checkConfigChange();
void loadConfig();
void setupComms();
void connectToMqtt();
void onMqttConnect(bool sessionPresent);
void onMqttDisconnect(AsyncMqttClientDisconnectReason reason);
void onMqttPublish(const uint16_t& packetId);
void onMqttMessage(char* topic, char* payload, const AsyncMqttClientMessageProperties& properties,
                   const size_t& len, const size_t& index, const size_t& total);
void onMqttSubscribe(const uint16_t& packetId, const uint8_t& qos);
void onMqttUnsubscribe(const uint16_t& packetId);


void cancelTicker() 
{
  //keep LED off
  ticker.detach();    
  digitalWrite(pinLED, LED_OFF);
  ledActive = false;
}

void tick()
{
  //toggle state
  int state = !digitalRead(pinLED);  // reverse the current state of GPIO1 pin
  digitalWrite(pinLED, state);     // set pin to the opposite state
  // check for a limited count flash sequence concluding
  if(state == LED_OFF) {
    if(ledCount > 0) {
      ledCount--;
      if(ledCount == 0)
        cancelTicker();
    }
  }
}

void startTicker(float period, int count=0) 
{
#ifdef DEBUG_LED_FLASH  
  Serial.printf("Flash %d @ %.1f\n", count, period);
#endif
  digitalWrite(pinLED, LED_ON);     // set LED on
  ticker.attach(period, tick);
  ledCount = count;
  ledActive = true;
}


void startRepeatedStrobe(int count, float interval=0.1)
{
  startTicker(interval, count);                 // flash fast until timeout
}



void setup() 
{
  Serial.begin(115200);
  while(!Serial);
  Serial.println();
  pinMode(pinLED, OUTPUT);
  digitalWrite(pinLED, LED_OFF);


  pDRD = new DoubleResetDetector(DRD_TIMEOUT, DRD_ADDRESS);
  if (pDRD->detectDoubleReset()) 
  {
    Serial.printf("Double Reset Detected, #%d\n", pDRD->getCount());
    DRD_Detected = true;
  } 

  GUI.begin();
  bool initSTA = false;
  if(!configManager.begin()) 
  {
    // fresh system, force STA creds to be an Afterburner
    initSTA = true;
  }
  configManager.setConfigSaveCallback(configChangeCallback);

  loadConfig();

  if(DRD_Detected) 
  {
    if(pDRD->getCount() >= 9)   // 10 repeats
    {
      Serial.println("Forcing credentials for a typical Afterburner");
      initSTA = true;
      strcpy(configManager.data.target, "192.168.4.1");
      configManager.data.useMQTT = false;
      configManager.save();
      startTicker(0.7);
      WiFiManager.begin("AfterburnerRemoteTemp", 60000);
    }
    else if(pDRD->getCount() >= 4) // 5 repeats
    {
      Serial.println("Erasing credentials, enabling portal");
      forgetConfigCallback();
      WiFiManager.forget();
      startTicker(0.1);
      WiFiManager.begin("AfterburnerRemoteTemp", 0);
    }
    else {
      Serial.println("Enabling portal");
      startTicker(0.25);
      WiFiManager.begin("AfterburnerRemoteTemp", 0);
    }
  }
  else 
  {
    Serial.println("Normal boot, attempting STA");
    startTicker(0.7);
    WiFiManager.begin("AfterburnerRemoteTemp", 60000);
  }
  WiFiManager.forgetWiFiFunctionCallback(forgetConfigCallback);
  if(initSTA) 
  {
    WiFiManager.setNewWifi("Afterburner", "thereisnospoon");  // cleverly define a typical Afterburner upon intial setup!
  }

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);

  setupComms();
  mqttClient.setServer(mqttBroker, mqttPort);
  if(mqttUser[0]) 
  {
    mqttClient.setCredentials(mqttUser, mqttPass);
  }


#ifdef USE_WEBSOCKET
  setupWebSocket();
#endif

  sensors.begin();

  Serial.println("Setup concluded, starting loop");
}

void loop() 
{
  // async handlers / software interrupts
  pDRD->loop();
  WiFiManager.loop();
  configManager.loop();

  if(websocketUse)
    webSocket.loop();

  //your code here
  if(reqReading) {
    reqReading = false;
    getReading();
  }
  checkLED();
  checkReading();
}

void forgetConfigCallback()
{
  Serial.println("Forgeting stored STA credentials!");
  WiFi.enableSTA(true);
  WiFi.persistent(true);
  WiFi.disconnect(true);
  WiFi.persistent(false);
}

void configChangeCallback()
{
  checkConfigChange();
}

void checkLED(bool force)
{
  if(!WiFi.isConnected()) 
  {
    statusInfo.noSTA(WiFiManager.isCaptivePortal());
    if(!ledActive) {
      // if(WiFiManager.isCaptivePortal())
      // {
      //   startTicker(hasSSID() ? 0.25 : 0.1);
      // }
      startTicker(WiFiManager.isCaptivePortal() ? (hasSSID() ? 0.25 : 0.1) : 0.7);
    }
  }
}

float getReading() 
{
  sensors.requestTemperatures();
  float temp = sensors.getTempCByIndex(0);
  temp = float((int)((temp * 10) + 0.5)) * 0.1;
  temp += configManager.data.offset;
  statusInfo.reading = temp;
  statusInfo.offset = configManager.data.offset;
  return temp;
}

char outstr[32];

void checkReading()
{
  long currentMillis = millis();
  long tDelta = currentMillis - previousSample;

  if (tDelta >= sampleInterval) 
  {
    previousSample = currentMillis;

    float CurrentTemp = getReading();

    if(CurrentTemp == DEVICE_DISCONNECTED_C) {
      startRepeatedStrobe(8);  // no sensor alert via LED - 8 fast flashes
    }
    else 
    {
      if(WiFiManager.isCaptivePortal() || !WiFi.isConnected()) 
      {
        return;   // LED will already be flashing as a result of being in portal or no STA
      }

      if(websocketUse)
      {
        if(webSocket.isConnected()) 
        {
          startRepeatedStrobe(1);
          sprintf(outstr, "{\"TempRemote\":%.1f}", CurrentTemp);
          Serial.printf("[WS] Sending %s\n", outstr);
          if(!webSocket.sendTXT(outstr)) 
          {
            Serial.println("[WS] appears to have been lost :(");
            webSocket.disconnect();
          };
        }
        else 
        {
          startRepeatedStrobe(3);
        }
      }
      else 
      {
        if(mqttClient.connected()) 
        {
          startRepeatedStrobe(1);
          dtostrf(CurrentTemp, 4, 2, outstr);
          String topic = cmdTopic + "/TempRemote";
          Serial.printf("[MQTT] Sending %s as %s\n", topic.c_str(), outstr);
          mqttClient.publish(topic.c_str(), 0, false, outstr);
        }
        else 
        {
          startRepeatedStrobe(3);
        }
      }
    }
  }
}

bool hasSSID()
{
  struct station_config conf;
  wifi_station_get_config_default(&conf);
  return conf.ssid[0] != 0;
}

void createWSconnStateInfo(const char* insert) 
{
  sprintf(statusInfo.connState, "Websocket %s %s", insert, websocketIP);
  Serial.println(statusInfo.connState);
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) 
{
  static bool didConnect = false;
	switch(type) {
		case WStype_DISCONNECTED:
#ifdef WS_USE_SERIAL
			WS_USE_SERIAL.printf("[WSc] Disconnected!\n");
#endif
      createWSconnStateInfo(didConnect ? "disconnected from" : "failed to connect to");
      didConnect = false;
			break;
		case WStype_CONNECTED: 
#ifdef WS_USE_SERIAL
			WS_USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);
#endif
      didConnect = true;
      createWSconnStateInfo("connected to");
  	  break;
		case WStype_TEXT:
#ifdef WS_USE_SERIAL
			WS_USE_SERIAL.printf("[WSc] get text: %s\n", payload);
#endif
			// send message to server
			// webSocket.sendTXT("message here");
			break;
		case WStype_BIN:
#ifdef WS_USE_SERIAL
			WS_USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
#endif
			hexdump(payload, length);

			// send data to server
			// webSocket.sendBIN(payload, length);
			break;
    case WStype_PING:
      // pong will be send automatically
#ifdef WS_USE_SERIAL
      WS_USE_SERIAL.printf("[WSc] get ping\n");
#endif
      break;
    case WStype_PONG:
      // answer to a ping we send
#ifdef WS_USE_SERIAL
      WS_USE_SERIAL.printf("[WSc] get pong\n");
#endif
      break;
    default:
      break;
  }
}

void setupWebSocket()
{
  sprintf(statusInfo.connState, "Disconnected from %s", websocketIP);
	// server address, port and URL
	webSocket.begin(websocketIP, 80);

	// event handler
	webSocket.onEvent(webSocketEvent);

	// use HTTP Basic Authorization this is optional remove if not needed
	// webSocket.setAuthorization("user", "Password");

	// try ever 5000 again if connection has failed
	webSocket.setReconnectInterval(5000);
  
  // start heartbeat (optional)
  // ping server every 15000 ms
  // expect pong from server within 3000 ms
  // consider connection disconnected if pong is not received 2 times
#ifdef ENABLE_WEBSOCKET_PING
  // CAUTION: - OLD AFTERBURNERS DON'T RESPOND TO PINGS (prior V3.5.1.40 at least) :( 
  // this causes unwanted reconnects, which refreshes the JSON to all clients.
  // THAT then makes the BT App / web page difficult to work with
  webSocket.enableHeartbeat(10000, 3000, 2);  
#endif
}

void checkConfigChange()
{
  bool different = false;
  different |= websocketUse != !configManager.data.useMQTT;
  different |= strcmp(websocketIP, configManager.data.target) != 0;
  different |= strcmp(mqttBroker, configManager.data.broker) != 0;
  different |= mqttPort != configManager.data.port;
  different |= strcmp(mqttUser, configManager.data.uname) != 0;
  different |= strcmp(mqttPass, configManager.data.pword) != 0;
  different |= strcmp(mqttTopic, configManager.data.pword) != 0;

  if(different) 
  {
    loadConfig();
    setupComms();
  }
}

void setupComms()
{
  if(webSocket.isConnected()) 
    webSocket.disconnect();

  if(mqttClient.connected())
    mqttClient.disconnect();

  if(websocketUse) 
  {
    setupWebSocket();
  }
  else 
  {
    mqttClient.setServer(mqttBroker, mqttPort);
    if(mqttUser[0]) 
      mqttClient.setCredentials(mqttUser, mqttPass);
    connectToMqtt();
  }
}

void safeCopy(char* dest, const char* src, int len)
{
  strncpy(dest, src, len);
  dest[len-1] = 0;
}

void loadConfig()
{
  websocketUse = !configManager.data.useMQTT;
  safeCopy(websocketIP, configManager.data.target, sizeof(websocketIP));
  safeCopy(mqttBroker, configManager.data.broker, sizeof(mqttBroker));
  mqttPort = configManager.data.port;;
  safeCopy(mqttUser, configManager.data.uname, sizeof(mqttUser));
  safeCopy(mqttPass, configManager.data.pword, sizeof(mqttPass));
  safeCopy(mqttTopic, configManager.data.topic, sizeof(mqttTopic));

  cmdTopic = String(mqttTopic) + String("/cmd");
  onlineTopic = String(mqttTopic) + String("/status");

  statusInfo.method = websocketUse ? "Websocket" : "MQTT";
}


void connectToMqtt()
{
  if(!websocketUse) {
    if(WiFi.isConnected()) {
      sprintf(statusInfo.connState, "Connecting to broker @ %s...", mqttBroker);
      Serial.println(statusInfo.connState);
      mqttClient.connect();
    }
    else {
      statusInfo.noSTA();
    }
  }
}

void onMqttConnect(bool sessionPresent)
{
  sprintf(statusInfo.connState, "Connected to broker @ %s", mqttBroker);
  Serial.println(statusInfo.connState);
  mqttClient.subscribe(onlineTopic.c_str(), 0);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  (void) reason;

  if(reason == AsyncMqttClientDisconnectReason::MQTT_NOT_AUTHORIZED)
    strcpy(statusInfo.connState, "Incorrect credentials!");
  else if(reason == AsyncMqttClientDisconnectReason::MQTT_UNACCEPTABLE_PROTOCOL_VERSION)
    strcpy(statusInfo.connState, "Broker rejected TCP method!");
  else if(reason == AsyncMqttClientDisconnectReason::MQTT_SERVER_UNAVAILABLE)
    strcpy(statusInfo.connState, "Broker unavailable!");
  else if(reason == AsyncMqttClientDisconnectReason::MQTT_MALFORMED_CREDENTIALS)
    strcpy(statusInfo.connState, "Malformed credentials!");
  else
    strcpy(statusInfo.connState, "Not connected to broker!");
  Serial.println(statusInfo.connState);

  if (WiFi.isConnected())
  {
    mqttReconnectTimer.once(5, connectToMqtt);
  }
}

void onMqttSubscribe(const uint16_t& packetId, const uint8_t& qos)
{
  sprintf(statusInfo.connState, "Subscribed to %s", mqttTopic);
  Serial.println(statusInfo.connState);
  strcpy(statusInfo.abStatus, "Unknown, check topic prefix");
}

void onMqttUnsubscribe(const uint16_t& packetId)
{
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char* topic, char* payload, const AsyncMqttClientMessageProperties& properties,
                   const size_t& len, const size_t& index, const size_t& total)
{
  (void) payload;

  if(strcmp(topic, onlineTopic.c_str()) == 0) {
    if(len < sizeof(statusInfo.abStatus)) {
      strncpy(statusInfo.abStatus, payload, len);
      statusInfo.abStatus[len] = 0;
    }
    else {
      strcpy(statusInfo.abStatus, "Status message was too long?");
    }
    Serial.println(statusInfo.connState);
  }
}

void onMqttPublish(const uint16_t& packetId)
{
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}


String getReadingJSON()
{
  String JSON;
  StaticJsonDocument<200> jsonBuffer;

  if(statusInfo.reading == DEVICE_DISCONNECTED_C)
    jsonBuffer["reading"] = "No Sensor";
  else  
    jsonBuffer["reading"] = String(dtostrf(statusInfo.reading, 0, 1, outstr)) + "°C";
  serializeJson(jsonBuffer, JSON);

  return JSON;
}


String getStatusJSON()
{
  String JSON;
  StaticJsonDocument<400> jsonBuffer;

  if(statusInfo.reading == DEVICE_DISCONNECTED_C)
    jsonBuffer["reading"] = "No Sensor";
  else  
    jsonBuffer["reading"] = String(dtostrf(statusInfo.reading, 0, 1, outstr)) + "°C";
  jsonBuffer["offset"] = dtostrf(statusInfo.offset, 0, 2, outstr);
  jsonBuffer["method"] = statusInfo.method;
  jsonBuffer["abStatus"] = statusInfo.abStatus;
  jsonBuffer["connState"] = statusInfo.connState;
  serializeJson(jsonBuffer, JSON);

  return JSON;
}