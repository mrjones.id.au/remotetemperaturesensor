
# Afterburner Remote Temperature Sensor

## An ESP8266 project using an ESP-01S module with a DS18B20

![Demo setup](images/demo.png)

Temperature readings are sent via an STA<sub>(1)</sub> WiFi connection to a target Afterburner.  

The STA connection can be either directly to the Afterburner's own Access Point, or via a Wireless LAN.  
Either a websocket or MQTT connection can be used to send temperature readings to the target Afterburner.  
*(The Afterburner's AP<sub>(2)</sub> can only accept a websocket connection).*  

A new system is configured to connect to an Afterburner's Access Point.

A Dallas 1 wire digital thermometer is used for temperature sensing, attached to GPIO0 on an ESP-01S module.  
The ESP-01S module hosts an LED which is used for basic status feedback.  
Otherwise, only a 3.3V supply is required.

---

### Acknowledgements

Maakbaas for his brilliant redesign of the old unreliable WiFi Manager.
<https://maakbaas.com/esp8266-iot-framework/logs/framework-introduction/>

I have myself reworked the web server to provide the interactive features I desired, but his original work is a brilliant redesign.

---

### Compiling

The project must be built using PlatformIO.  
The WiFi Manager library is reliant upon this fact.  

Building using PlatformIO is by no means a hindrance. It is in fact a far better way to work than the Arduino IDE.

When the project folder is loaded by PlatformIO, it reads platformio.ini and will autiomatically install the required tools and compiler to build the application on your system.

It is only a case of adapting the 'upload_port' definition in platformio.ini to suit your system's COM port (typ. a USB ESP01 PROG tool).

You can then select Upload from the PIO menu and the project will be built and uploaded to the target hardware.

---

### Configuration

By default, when starting as a virgin system, the system is automatically configured to connect to an Afterburner's Access Point.  
| Parameter | Value |
| :--- | :--- |
| SSID | Afterburner |
| Password | thereisnospoon |
| Websocket IP | 192.168.4.1 |

A WiFi Manager provides an inbuilt web page that allows configuration of the SSID and password required to establish the fundamental STA connection.  
The STA password can be entered on the web page to configure the system but is not provided to the web page when it is loaded. Clicking upon "show" will instead present \*\*&nbsp;Suppressed&nbsp;\*\*. If you do enter a password, it can be made visible at the time.  
The WiFi Manager's web page also allows configuration of the data connection, and offset of the sensor if required.  

If the STA WiFi is unavailable at boot time, the device will default to providing its own AP to allow configuration.  
*SSID: AfterburnerRemoteTemp, a password is not required.*  

A captive portal is created on the AP, so if you are using a mobile device it should bring up the configuration web page automatically.<br>
Otherwise, access <http://192.168.4.1> via a web browser.

When an STA connection is established, the device's own AP is closed.

The device's AP can also be forced to run by utilising a double reset:  

- A single reset will boot the unit normally.
- If reset twice in quick succession, the LED will flash at a medium rate (\~2 times a second), indicating the AP is running.
- If reset 5 times in quick succession, any stored STA credentials will be erased and the LED will flash faster (\~5 times a second), indicating the AP is running and no STA credentials exist.  
Future resets will return to this state until credentials are entered.
- If reset 10 times in quick succession, the stored STA credentials will be reset to an Afterburner's Access Point.  
i.e Afterburner / thereisnospoon / websocket @ 192.168.4.1.
Future resets will return to this state until credentials are entered.
- **The double reset window expires after 5 seconds.**

---

### Operation

After a normal reset, the LED will flash slowly (\~once every 1.5 seconds) as STA is being attempted, then a single strobe once every 4 seconds or so as readings are sent.

If the unit has connected OK to the STA connection, the LED will be strobed each 4 seconds in the following manner:  

- 1 strobe: - Normal operation, readings are being sent.
- 3 strobes: - STA is connected, but the connection to either the MQTT broker or Afterburner's websocket cannot be achieved.
- 8 strobes: - STA is connected, but the DS18B20 sensor is not responding (check wiring).

Constant flashing (\~2 times a second, or faster) indicates the AP is running, you can reconfigure the unit by connecting to the devices AP, then accessing <http://192.168.4.1>.  
As a captive portal is created on the AP, if you use a mobile device it should bring up the configuration web page automatically.

Rapid flashing (\~5 times a second) indicates no STA credentials are stored, and thus the unit is not able to connect and send readings.  
The device MUST be configured.

---

### Programming

The ESP-01S does not include any bootload assist components.  
However, it is easy to obtain suitable ESP-01S USB adapters, preferebaly the style pictured.  
This style includes transitors that allow fully automatic programming via esp-tool by exercising GPIO0 as required.

![ESP01S-PROG](images/esp01s-prog.png)  ESP-01S Programmer

## Construction

#### 3.3V Power Supply

The ESP-01S module can **only** accept a 3.3V power supply.  
You must furnish a proper 3.3V supply.  Ideally if dropping from 12V or simialr, use a buck regulator for power conversion efficiency reasons.

Lower input voltages (eg 5V) would be best regulated by using a low voltage cutout 3.3V regulator, eg AMS1117.

For initial testing, the ESP-01S USB programmer is ideal as it also included header pins to which the DS18B20 can be attached.

#### DS18B20 digital temperature sensor

The ESP-01S module already includes a 10k pullup on GPIO0.  
Thus a DS18B20 can be attached by direct wiring:
| DS18B20 | ESP-01S |
| :---: | :---: |
| Vcc | 3.3V |
| GND | GND |
| Data | GPIO0 |

###### DS18B20  
![DS18B20](images/ds18b20.png)

###### Wiring

![Wiring](images/esp01s-pinout.png)

---

##### Notes

1. STA - Wifi Station. A unit that connects to another Wifi Access Point.  
2. AP - Access Point. A unit that offers a Wifi Connection other units can join.
