import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route, NavLink} from "react-router-dom";
import { FiThermometer as HeaderIcon } from "react-icons/fi";

import { GlobalStyle, Menu, Header, Page, Hamburger} from "./comp/UiComponents";
import { WifiPage } from "./comp/WifiPage";
import { ConfigPage } from "./comp/ConfigPage";
import { StatusPage } from "./comp/StatusPage";

import { bin2obj } from "./functions/configHelpers";

import Config from "./configuration.json";

let loc;
if (Config.find(entry => entry.name === "language")) {
  loc = require("./lang/" + Config.find(entry => entry.name === "language").value + ".json");
} else {
  loc = require("./lang/en.json");
}

let url = "http://192.168.20.118";
if (process.env.NODE_ENV === "production") {url = window.location.origin;}

if (process.env.NODE_ENV === "development") {require("preact/debug");}

// const displayData = new Array();

let binSize = 0;
let configData = new Object();

function Root() {
    
  const [menu, setMenu] = useState(false);
  const [newConfig, setNewConfig] = useState(0);

  useEffect(() => {
    fetchConfig();
  }, []);

  function fetchConfig() {
    fetch(`${url}/api/config/get`)
      .then((response) => {
        return response.arrayBuffer();
      })
      .then((data) => {
        console.log("config data received from server");
        binSize = data.byteLength;
        configData = bin2obj(data, Config);
        setNewConfig(newConfig+1);
      });
  }

  console.log("config", configData);
  let projectName = configData["projectName"];
  if (typeof projectName === "undefined") {
    projectName = Config.find(entry => entry.name === "projectName") ? Config.find(entry => entry.name === "projectName").value : "Remote Temperature Node";
  }

  let projectVersion = configData["projectVersion"];
  if (typeof projectVersion === "undefined") {
    projectVersion = Config.find(entry => entry.name === "projectVersion") ? Config.find(entry => entry.name === "projectVersion").value : "";
  }
  
  return <><GlobalStyle />

    <BrowserRouter>

      <Header>
        <h1><HeaderIcon style={{verticalAlign:"-0.1em"}} /> {projectName} {projectVersion}</h1>

        <Hamburger onClick={() => setMenu(!menu)} />
        <Menu className={menu ? "" : "menuHidden"}>
          <li><NavLink onClick={() => setMenu(false)} exact to="/">{loc.titleWifi}</NavLink></li>
          <li><NavLink onClick={() => setMenu(false)} exact to="/config">{loc.titleConf}</NavLink></li>
          <li><NavLink onClick={() => setMenu(false)} exact to="/status">{loc.titleStatus}</NavLink></li>
        </Menu>

      </Header>
  
      <Page>
        <Switch>
          <Route exact path="/config">
            <ConfigPage 
              API={url} 
              configData={configData}
              binSize={binSize}
              requestUpdate={fetchConfig}
            />
          </Route>
          <Route exact path="/status">
            <StatusPage 
              API={url} 
              configData={configData}
            />
          </Route>
          <Route path="/">
            <WifiPage API={url} />
          </Route>
        </Switch>
      </Page>

    </BrowserRouter>
  </>;

}


ReactDOM.render(<Root />, document.getElementById("root"));