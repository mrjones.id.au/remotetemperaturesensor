import React, { useState, useEffect, useLayoutEffect, useImperativeHandle, forwardRef } from "react";
import PropTypes from "prop-types";

import Config from "../configuration.json";
import { obj2bin } from "../functions/configHelpers";

import { Form, Button, Spinner } from "./UiComponents";
import { SensorItem, DashboardItems } from "./DashboardItems";

let loc;
if (Config.find(entry => entry.name === "language")) {
  loc = require("./../lang/" + Config.find(entry => entry.name === "language").value + ".json");
} else {
  loc = require("./../lang/en.json");
}

let confItems;
let prev;

export function ConfigPage(props) {
  
  const [sensor, setSensor] = useState({ reading: "No data"});
  const [spin, setSpin] = useState(false);

  useEffect(() => {
    document.title = loc.titleConf;
    getReading();
    console.log("sensor timer started");
    const ticker = setInterval(getReading, 5000);
    return () => {
      console.log("sensor timer killed");
      clearInterval(ticker);
    }
  }, []);

  function getReading()
  {
    fetch(`${props.API}/api/sensor/get`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log("sensor(cfg)", data);
        setSpin(false);
        setSensor(data);
      })
      .catch(error => {
        console.log("catch", data);
        clearInterval(ticker);//throw(error);
      }
    );
  };

  function setConfig()
  {
    fetch(`${props.API}/api/config/set`, {
      method: "post",
      body: form2bin(),                
    })
    .then((response) => { 
      return response.status; 
    })
    .then((status) => {
      console.log("cfgset");
      if (status == 200) {props.requestUpdate();}
    })
  };

  if(prev != props) {
    confItems = <DashboardItems items={Config} data={props.configData} />;    
    console.log("confItems change");
    prev = props;
  }

  function buttonfn() 
  {
    setConfig();
    const newval = Number(document.getElementById("offset").value).toFixed(1);
    const oldval = Number(props.configData.offset).toFixed(1);
    console.log(newval, oldval);
    if(newval != oldval) 
    {
      setSpin(true);
      setTimeout(getReading, 1500);
    }
  };

  let button;
  if (Object.keys(props.configData).length > 0) {
    button = <Button onClick={buttonfn}>{loc.globalSave}</Button>;
  }

  const read = spin ? <Spinner /> : sensor.reading;
  const item = SensorItem(read);
  
  const form = <>
    <Form>
      {confItems}
    </Form>
    <Form>
      {item}
    </Form>
    <hr />
    {button}        
  </>;

  return <><h2>{loc.titleConf}</h2><p>{form}</p></>;

  function form2bin() {
    const newData = {};

    for (let i = 0; i < Config.length; i++) {
      if (Config[i].hidden) {
        newData[Config[i].name] = props.configData[Config[i].name];
        continue;
      }

      switch (Config[i].type) {
        case "bool":
          newData[Config[i].name] = document.getElementById(Config[i].name).checked;
          break;
        
        default:
          if (Config[i].type != "separator" && Config[i].type != "label" && Config[i].type != "header") {
            newData[Config[i].name] = document.getElementById(Config[i].name).value;
          }
      }
    }
    
    return obj2bin(newData, props.binSize, Config);
      
  };
}

ConfigPage.propTypes = {
  API: PropTypes.string,
  binSize: PropTypes.number,
  configData: PropTypes.object,
  requestUpdate: PropTypes.func,
  newConfig: PropTypes.number,
};
