import React, { useState, useEffect } from "react";

import { Form, Button, Spinner, Confirmation } from "./UiComponents";
import { FiLink as Link, FiCornerDownRight as CornerDownRight, FiServer as Server, FiThermometer as Thermometer } from "react-icons/fi";

import Config from "../configuration.json";
let loc;
if (Config.find(entry => entry.name === "language")) {
  loc = require("./../lang/" + Config.find(entry => entry.name === "language").value + ".json");
} else {
  loc = require("../lang/en.json");
}


export function StatusPage(props) {

  const [state, setState] = useState({ method: "websocket", connState:"ready", abStatus:"online", reading:-127, offset:0});

  useEffect(() => {
    document.title = loc.titleStatus;
    fetchStatus();
    console.log("status timer started");
    const ticker = setInterval(fetchStatus, 2500);
    return () => {
      console.log("status timer killed");
      clearInterval(ticker);
    }
  }, []);

  function fetchStatus() {
    fetch(`${props.API}/api/status/get`)
      .then((response) => {
        return response.json();
      })
      .then((jsonBody) => {
        setState(jsonBody);
      });
  };

  console.log("status", state);

  let ABstat;
  if(props.configData.useMQTT) {
    ABstat = <>
      <p>
        <label htmlFor="mqtt"><Server /> {loc.statusABstat}:</label>
        <label id="abstate"> {state.abStatus}</label>   
      </p>   
    </>;
  }

  let form1 = <>
    <Form>
      <p>
        <label htmlFor="connection"><Link /> {loc.statusMethod}:</label>
        <label id="method"> {state.method}</label>   
      </p>
      <p>
        <label htmlFor="status"><CornerDownRight /> {loc.statusStatus}:</label>
        <label class="wide" id="state"> {state.connState}</label>   
      </p>   
      {ABstat}
    </Form>
  </>;
    
  
  const ofs = (state.offset < 0 ? '' : '+') + Number(state.offset).toFixed(1);
  let cls = state.reading;
  let form2 = <>
    <Form>
      <p>
        <label htmlFor="sensor"><Thermometer /> {loc.statusReading}:</label>
        <label id="reading" class="wide"> {cls}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>({ofs})</i></label>   
      </p>
    </Form>
  </>;

  let page = <><h2>{loc.titleStatus}</h2> 
    <h3>{loc.statusConnection}</h3>{form1}
    <hr />
    <h3>{loc.statusReading}</h3>{form2}</>;
    
  return page;
}

