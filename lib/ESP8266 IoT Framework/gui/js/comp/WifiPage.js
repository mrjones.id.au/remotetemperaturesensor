import React, { useState, useEffect } from "react";

import { Form, Button, Spinner, Confirmation } from "./UiComponents";
import { FiWifi as Wifi, FiLock as Lock, FiServer as Server, FiCornerDownRight as CornerDownRight } from "react-icons/fi";

import Config from "./../configuration.json";
let loc;
if (Config.find(entry => entry.name === "language")) {
  loc = require("./../lang/" + Config.find(entry => entry.name === "language").value + ".json");
} else {
  loc = require("./../lang/en.json");
}

let testDhcp = false;
let suppressed = "** Suppressed **";

export function WifiPage(props) {
  const [state, setState] = useState({ ssid: [], pass:[]});
  const [forgetModal, setForgetModal] = useState(false);
  const [saveModal, setSaveModal] = useState(false);
  const [dhcpForm, setDhcpForm] = useState(true);
  const [showPwd, setShowPwd] = useState(false);

  useEffect(() => {
    document.title = loc.titleWifi;
    fetch(`${props.API}/api/wifi/get`)
      .then((response) => {
        return response.json();
      })
      .then((jsonBody) => {
        setState(jsonBody);
        testDhcp = true;  // perform test when rendering page
      });
  }, []);

  function passParam()
  {
    if(state.pass != undefined && state.pass != suppressed)
      return '&' + getParam('pass');
    else 
      return "";
  }

  function getParam(id)
  {
    let value = document.getElementById(id).value.trim();
    return id + '=' + encodeURIComponent(value);
  }

  function changeWifi() 
  {
    let api;
    if (dhcpForm) {
      api = `${props.API}/api/wifi/set?${getParam("ssid")}${passParam()}`;
    } else {
      api = `${props.API}/api/wifi/setStatic?${getParam("ssid")}${passParam()}&${getParam("ip")}&${getParam("sub")}&${getParam("gw")}&${getParam("dns")}`;
    }
    console.log("POST:", api);
    fetch(api, { method: "POST" });
  }

  function changeSpecificState(id, val) {
    let work = state;
    work[id] = val;
    setState(work);
  }

  const handleChange = event => {
    changeSpecificState(event.target.name, event.target.value);
  }

  function togglePwd() {
    setShowPwd(!showPwd);
  }


  console.log("state", state);
  if(testDhcp) {
    setDhcpForm(state.ip == undefined);
  }
  testDhcp = false;
  
  let dhcp = <></>;

  if (!dhcpForm) 
  {
    dhcp = <>
      <p>
        <label htmlFor="ip"><CornerDownRight /> {loc.wifiIP}:</label>
        <input type="text" id="ip" name="ip" autoCapitalize="none" value={state.ip} onBlur={handleChange}/>
      </p>
      <p>
        <label htmlFor="sub"><CornerDownRight /> {loc.wifiSub}:</label>
        <input type="text" id="sub" name="sub" autoCapitalize="none" value={state.sub} onBlur={handleChange}/>
      </p>
      <p>
        <label htmlFor="gw"><CornerDownRight /> {loc.wifiGW}:</label>
        <input type="text" id="gw" name="gw" autoCapitalize="none" value={state.gw} onBlur={handleChange}/>
      </p>
      <p>
        <label htmlFor="dns"><CornerDownRight /> {loc.wifiDNS}:</label>
        <input type="text" id="dns" name="dns" autoCapitalize="none" value={state.dns} onBlur={handleChange}/>
      </p>
    </>;
  }    

  if(state.pass == undefined) {
    changeSpecificState("pass", suppressed);
  }

  let pwField;
  if(showPwd) {
    pwField = <>
      <input type="text" id="pass" name="pass" autoCapitalize="none" value={state.pass} onBlur={handleChange}/>
      &nbsp;<button type="button" onClick={() => togglePwd()}>{loc.wifiHidePW}</button>
    </>
  }
  else {
    pwField = <>
      <input type="password" id="pass" name="pass" autoCapitalize="none" value={state.pass} onBlur={handleChange}/>
      &nbsp;<button type="button" onClick={() => togglePwd()}>{loc.wifiShowPW}</button>
    </>
  }

  let form = <>
    <Form>
      <p>
        <label htmlFor="ssid"><Wifi /> {loc.wifiSSID}:</label>
        <input type="text" id="ssid" name="ssid" autoCapitalize="none" value={state.ssid} onBlur={handleChange}/>
      </p>
      <p>
        <label htmlFor="pass"><Lock /> {loc.wifiPass}:</label>
        {pwField}
      </p>   
      <p>
        <label htmlFor="dhcp"><Server /> {loc.wifiDHCP}:</label>
        <input type="checkbox" id="dhcp" name="dhcp" checked={dhcpForm} onChange={()=>setDhcpForm(!dhcpForm)} />
      </p>
      {dhcp}      
    </Form>
    <Button onClick={() => setSaveModal(true)}>{loc.globalSave}</Button>
  </>;
    

  let page = <><h2>{loc.titleWifi}</h2> 
    <h3>{loc.globalStatus}</h3></>;
    
  let knownSsidInfo;
  if (state.ssid == "") knownSsidInfo = loc.wifiCP;
  else knownSsidInfo = <>{loc.wifiConn} {state.ssid}  (<a onClick={() => setForgetModal(true)}>{loc.wifiForget}</a>)</>;
    
  page = <>{page}<p>{knownSsidInfo == null ? <Spinner /> : knownSsidInfo}</p></>;

  page = <>{page}<h3>{loc.wifiUpdate}</h3>{form}
    <Confirmation active={forgetModal}
      confirm={() => { fetch(`${props.API}/api/wifi/forget`, { method: "POST" }); setForgetModal(false); }}
      cancel={() => setForgetModal(false)}>{loc.wifiModal1}</Confirmation>
    <Confirmation active={saveModal}
      confirm={() => { changeWifi(); setSaveModal(false); }}
      cancel={() => setSaveModal(false)}>{loc.wifiModal2}</Confirmation>
  </>;

  return page;
}

