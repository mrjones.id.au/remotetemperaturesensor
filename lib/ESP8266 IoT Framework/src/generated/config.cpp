#include <Arduino.h>
#include "config.h"

uint32_t configVersion = 21484674; //generated identifier to compare config with EEPROM

const configData defaults PROGMEM =
{
	"192.168.4.1",
	false,
	"broker.hivemq.com",
	1883,
	"",
	"",
	"",
	0,
	"en"
};