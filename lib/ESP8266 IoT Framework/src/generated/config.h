#ifndef CONFIG_H
#define CONFIG_H

struct configData
{
	char target[32];
	bool useMQTT;
	char broker[32];
	uint16_t port;
	char uname[32];
	char pword[32];
	char topic[32];
	float offset;
	char language[3];
};

extern uint32_t configVersion;
extern const configData defaults;

#endif