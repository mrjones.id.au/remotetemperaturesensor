#include "webServer.h"
#include "ArduinoJson.h"

// Include the header file we create with webpack
#include "generated/html.h"

//Access to other classes for GUI functions
#include "WiFiManager.h"
#include "configManager.h"

extern bool reqReading;
extern String getReadingJSON();
extern String getStatusJSON();

void webServer::begin()
{
  //to enable testing and debugging of the interface
  DefaultHeaders::Instance().addHeader(PSTR("Access-Control-Allow-Origin"), PSTR("*"));

  server.addHandler(&ws);
  server.begin();

  server.onNotFound(requestHandler);

  bindAll();
}

void webServer::bindAll()
{
  //Restart the ESP
  server.on(PSTR("/api/restart"), HTTP_POST, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), ""); //respond first because of restart
    ESP.restart();
  });

  //update WiFi details
  server.on(PSTR("/api/wifi/set"), HTTP_POST, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), ""); //respond first because of wifi change
    if(request->hasArg("pass"))
      WiFiManager.setNewWifi(request->arg("ssid"), request->arg("pass"));
    else {
      struct station_config conf;
      wifi_station_get_config_default(&conf);

      WiFiManager.setNewWifi(request->arg("ssid"), (const char*)conf.password); 
    }
  });

  //update WiFi details with static IP
  server.on(PSTR("/api/wifi/setStatic"), HTTP_POST, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), ""); //respond first because of wifi change
    if(request->hasArg("pass"))
      WiFiManager.setNewWifi(request->arg("ssid"), request->arg("pass"), request->arg("ip"), request->arg("sub"), request->arg("gw"), request->arg("dns"));
    else {
      struct station_config conf;
      wifi_station_get_config_default(&conf);

      WiFiManager.setNewWifi(request->arg("ssid"), (const char*)conf.password, request->arg("ip"), request->arg("sub"), request->arg("gw"), request->arg("dns"));
    }
  });

  //update WiFi details
  server.on(PSTR("/api/wifi/forget"), HTTP_POST, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), ""); //respond first because of wifi change
    WiFiManager.forget();
  });

  //get WiFi details
  server.on(PSTR("/api/wifi/get"), HTTP_GET, [](AsyncWebServerRequest *request) {
    String JSON;
    StaticJsonDocument<200> jsonBuffer;

    struct station_config conf;
    wifi_station_get_config_default(&conf);

    jsonBuffer["ssid"] = (const char*)conf.ssid; // WiFiManager.SSID();  
    // jsonBuffer["pass"] = (const char*)conf.password;  // do not send password, only accept a change returned
    IPAddress ip = configManager.internal.ip;
    if(ip.isSet()) {
      jsonBuffer["ip"] = ip.toString();
      ip = configManager.internal.sub;
      jsonBuffer["sub"] = ip.toString();
      ip = configManager.internal.gw;
      jsonBuffer["gw"] = ip.toString();
      ip = configManager.internal.dns;
      jsonBuffer["dns"] = ip.toString();
    }
    serializeJson(jsonBuffer, JSON);

    request->send(200, PSTR("text/html"), JSON);
#ifdef SHOWGETS
    Serial.print("wifi get sent");
    Serial.println(JSON);
#endif
  });

//get sensor reading
  server.on(PSTR("/api/sensor/get"), HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), getReadingJSON());
#ifdef SHOWGETS
    Serial.print("sensor get sent");
    Serial.println(JSON);
#endif
  });

  //get status info
  server.on(PSTR("/api/status/get"), HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, PSTR("text/html"), getStatusJSON());
#ifdef SHOWGETS
    Serial.print("status get sent");
    Serial.println(JSON);
#endif
  });

  //send binary configuration data
  server.on(PSTR("/api/config/get"), HTTP_GET, [](AsyncWebServerRequest *request) {
#ifdef SHOWGETS
    Serial.println("/api/config/get");
#endif
    AsyncResponseStream *response = request->beginResponseStream(PSTR("application/octet-stream"));
    response->write(reinterpret_cast<char*>(&configManager.data), sizeof(configManager.data));
    request->send(response);
  });

  //receive binary configuration data from body
  server.on(
    PSTR("/api/config/set"), HTTP_POST,
    [this](AsyncWebServerRequest *request) {},
    [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {},
    [this](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        
      static uint8_t buffer[sizeof(configManager.data)];
      static uint32_t bufferIndex = 0;

      for (size_t i = 0; i < len; i++)
      {
        buffer[bufferIndex] = data[i];
        bufferIndex++;
      }

      float now = configManager.data.offset;
      if (index + len == total)
      {
        bufferIndex = 0;
        configManager.saveRaw(buffer);
        if(now != configManager.data.offset) {        // sensor offset changed, request a reading now
                                                      // this must be asynchronous otherwise web server glues up
          reqReading = true;
        }
        request->send(200, PSTR("text/html"), "");
      }

  });
 
}

// Callback for the html
void webServer::serveProgmem(AsyncWebServerRequest *request)
{    
  // Dump the byte array in PROGMEM with a 200 HTTP code (OK)
  AsyncWebServerResponse *response = request->beginResponse_P(200, PSTR("text/html"), html, html_len);

  // Tell the browswer the content is Gzipped
  response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
  
  request->send(response);    
}

webServer GUI;