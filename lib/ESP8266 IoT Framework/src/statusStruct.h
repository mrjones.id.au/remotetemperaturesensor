#pragma once

#include <string.h>

struct sStatusStruct 
{
  const char* method;
  char connState[64];
  char abStatus[32];
  float reading;
  float offset;
  void noSTA(bool portal=false) {
    strcpy(connState, portal ? "Configuration Portal Active" : "STA is not connected");
    strcpy(abStatus, "");
  };
  sStatusStruct() {
    method = "not set";
    noSTA();
    reading = -127;
    offset = 0;
  }
};

